#!/bin/bash
# vidcost
#   given amount of plaing time in minutes and # of average subscribers,
#   it calculates how much the Opentok bill would be.
#
# 10 minutes of music
# 20 subscribers

# Video (Opentok) cost breakdown:
#   Cost per subscriber: $0.0475 = 10 minutes x 0.00475 (per subscriber minute)
#   Cost per 20 subscribers for 10 minutes: $0.95 = $0.0475 * 20

# opentok pricing
# TODO: more minutes cost less. need to figure this into the algorithm
cost_per_min="0.0475"

if [ "$#" -ne 2 ]; then
    echo "Error: need exactly 2 parameters"
    echo "  Usage: ./vidcost num_minutes num_subscribers"
    echo "  Example: ./vidcost 60 40"
    exit 1
fi

num_minutes=$1
num_subscribers=$2

# calculate that shiz
cost_per_subscriber=$(echo "$num_minutes * $cost_per_min" | bc)
total_opentok_cost=$(echo "$num_subscribers * $cost_per_subscriber" | bc)

echo "---- Summary ----"
echo "Performance time: $num_minutes minutes"
echo "Cost per subscriber: $cost_per_subscriber"
echo "Total Opentok bill: $total_opentok_cost"
