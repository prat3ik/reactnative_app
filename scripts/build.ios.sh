#!/bin/bash
# Build steps for iOS. Run this after you first clone the micturn project to
# install all dependencies and set things up. After that, just use "yarn run ios" to fire up the ios app.

echo "[INFO] Installing dependencies for MicTurn and running it for iOS."
echo "[INFO] Cleaning cache for watchman, react, haste, yarn, and npm"
watchman watch-del-all && rm -rf $TMPDIR/react-* && rm -rf $TMPDIR/haste* && rm -rf node_modules

echo "[INFO] running yarn install"
yarn install

echo "[INFO] adding $(pwd)/node_modules/.bin to your PATH"
path_line="export PATH=$(pwd)/node_modules/.bin:\$PATH"
grep -q -F "$path_line" ~/.bash_profile || echo $path_line >> ~/.bash_profile

echo "[INFO] Installing Pods"
cd ios && pod install
cp $firebasefile .

echo "[INFO] Running react-native link"
cd .. && react-native link

echo "[INFO] Starting with 'yarn run ios'"
yarn run ios

echo "[INFO] Build script completed, hopefully successfully."
echo "[INFO] Going forward, you can just run your app with 'yarn run ios' as all setup has been taken care of."
