# MicTurn React Native App

[![Build Status](https://app.bitrise.io/app/83395464c00168cb/status.svg?token=nbCGKCz60uVR82Ceb0RyiQ&branch=master)](https://app.bitrise.io/app/83395464c00168cb)

## Project Links

- [Trello](https://trello.com/b/Ga99lyev/mobile-app-dev) - for project management and discussions
- [MicTurn Firebase project](https://console.firebase.google.com/project/micturn-1303)
- [Wireframe](https://xd.adobe.com/view/d650d94b-9e07-484f-49aa-17e808e46b65-a061/) (password: Chicken1)

## Setup Your Development Environment

### iOS


1. Set up Firebase config file


NOTE: this step is currently not needed. We have the Firebase config file with api key committed to git, but we need to get this out of the repo at some point!

```
mv ios/GoogleService-Info.plist.example ios/GoogleService-Info.plist
```

Replace `__YOUR_FIREBASE_API_KEY__` in `ios/GoogleService-Info.plist` with the Firebase API key obtained from the [Firebase console](https://console.firebase.google.com/u/0/project/micturn-1303/overview).

2. Build it:

Currently, there are only steps for the iOS build:

```
git clone git@github.com:snobear/micturn.git
cd micturn
npm run build-ios
```

After you've done the first build with those steps ^, you can just run your app normally going forward with:

```
yarn run ios
```

**NOTES**

* You may get some `node-pre-gyp ERR!` lines which are safe to ignore
* If you have trouble with these steps, try running each of the commands found in the build script: `scripts/build.sh`

### Android

TODO

## Troubleshooting

See wiki - https://github.com/snobear/micturn/wiki/Troubleshooting-dev-environment-setup
