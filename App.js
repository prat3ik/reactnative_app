import React from 'react';
import firebase from 'react-native-firebase';
import { StyleSheet, Platform, Image, Text, View, ScrollView } from 'react-native';
import { Provider } from 'react-redux';
import store from './app/config/store';
import Navigator from './app/config/navigation';

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.props = props;
  }

  render() {
    return (
      <Provider store={store}>
        <Navigator />
      </Provider>
    );
  }
}
