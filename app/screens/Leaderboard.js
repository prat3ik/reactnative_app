import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  TouchableOpacity,
} from 'react-native';
import { leaders } from '../actions/leaderBoardActions';
import Spinner from '../components/SpinnerComponent/Spinner';

class Leaderboard extends React.Component {
  componentWillMount() {
    this.props.leaders();
  }

  render() {
    return (
      <View>
        <FlatList
          data={Object.values(this.props.leaderResults)}
          keyExtractor={(x, i) => i.toString()}
          renderItem={({ item }) =>
            (
              <Text style={{
                color: 'black', fontSize: 28, fontWeight: 'bold', marginVertical: 10,
              }}>
                {item.fullName}
              </Text>
            )
          } />
        <TouchableOpacity style={{ backgroundColor: 'yellow', height: 100, width: 100 }} onPress={() => this.props.navigation.navigate('Home')}>
          <Text>main screen</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

Leaderboard.propTypes = {
  leaders: PropTypes.func.isRequired,
  leaderResults: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const mapStateToProps = state => ({
  leaderResults: state.leaderBoard.leaderboard,
});

export default connect(mapStateToProps, { leaders })(Leaderboard);
