import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import MenuIcon from '../components/MenuIconComponent/MenuIcon';
import Image from '../components/ImageComponent/Image';
import { uploadImage } from '../helpers/StorageHelper';
import Input from '../components/InputComponent/Input';
import Button from '../components/Button/Button';
import Message from '../components/MessageComponent/Message';
import Spinner from '../components/SpinnerComponent/Spinner';
import { storeProfileData, getProfileData, profileDataPending, profileImageClicked } from '../actions/userProfileActions';

class MyProfile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      userId: firebase.auth().currentUser.uid,
    };
  }

  componentWillMount() {
    this.props.getProfileData(this.state.userId);
  }

  // handles updating initial state
  componentDidMount() {
    this.setState({
      fullName: this.props.fullName,
      website: this.props.website,
      twitter: this.props.twitter,
      location: this.props.location,
      imageUrl: this.props.imageUrl,
      loading: this.props.loading,
      success: this.props.success,
      error: this.props.error,
    });
  }

  // handles updating state in real-time
  componentWillReceiveProps(nextProps) {
    if (nextProps.loading) {
      this.setState({
        loading: nextProps.loading,
      });
    } else if (this.state.imageUrl !== nextProps.imageUrl &&
      nextProps.fullName == null) {
      this.setState({
        imageUrl: nextProps.imageUrl,
        loading: nextProps.loading,
      });
    } else {
      this.setState({
        fullName: nextProps.fullName,
        website: nextProps.website,
        twitter: nextProps.twitter,
        location: nextProps.location,
        imageUrl: nextProps.imageUrl,
        loading: nextProps.loading,
        success: nextProps.success,
        error: nextProps.error,
      });
    }
  }

  storeProfileData() {
    const myProfile = {
      userId: this.state.userId,
      fullName: this.state.fullName,
      website: this.state.website,
      twitter: this.state.twitter,
      location: this.state.location,
      imageUrl: this.state.imageUrl,
    };

    this.props.storeProfileData(myProfile);
  }

  profileImageClicked() {
    this.props.profileImageClicked(this.state.userId);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.myProfileView}>

          {/* hero/header component? */}
          <Text style={{ color: '#f1d746', fontSize: 30 }}>
            My Profile
          </Text>

          {
            this.state.loading
              ? <Spinner userProfileImage />
              : <Image source={this.state.imageUrl} onPress={() => this.profileImageClicked()} />
          };

          <View style={styles.inputView}>
            <Input
              value={this.state.fullName}
              onChangeText={fullName => this.setState({ fullName })}
              placeholder="Full Name" />
            <Input
              value={this.state.website}
              onChangeText={website => this.setState({ website })}
              placeholder="Website" />
            <Input
              value={this.state.twitter}
              onChangeText={twitter => this.setState({ twitter })}
              placeholder="Twitter" />
            <Input
              value={this.state.location}
              onChangeText={location => this.setState({ location })}
              placeholder="Location" />
          </View>

          {this.state.success ? <Message name="success" message="Successfully Updated Profile!" /> : null };
          {this.state.error ? <Message name="error" message={this.state.error} /> : null };

          <View style={styles.saveBtnContainer}>
            <Button profileSave text="Save" onPress={() => this.storeProfileData()} />
          </View>

        </View>

        <View style={styles.iconView}>
          <MenuIcon name="remove" onPress={() => this.props.hideProfile()} />
          <MenuIcon name="user" />
          <MenuIcon name="gear" />
        </View>

      </View>
    );
  }
}

MyProfile.defaultProps = {
  fullName: null,
  website: null,
  twitter: null,
  location: null,
  imageUrl: null,
  loading: true,
  success: false,
  error: null,
};

MyProfile.propTypes = {
  getProfileData: PropTypes.func.isRequired,
  storeProfileData: PropTypes.func.isRequired,
  hideProfile: PropTypes.func.isRequired,
  profileImageClicked: PropTypes.func.isRequired,
  fullName: PropTypes.string,
  website: PropTypes.string,
  twitter: PropTypes.string,
  location: PropTypes.string,
  imageUrl: PropTypes.string,
  loading: PropTypes.bool,
  success: PropTypes.bool,
  error: PropTypes.string,
};

const mapStateToProps = state => ({
  fullName: state.userProfiles.profile.fullName,
  website: state.userProfiles.profile.website,
  twitter: state.userProfiles.profile.twitter,
  location: state.userProfiles.profile.location,
  imageUrl: state.userProfiles.profile.imageUrl,
  loading: state.userProfiles.isLoading,
  error: state.userProfiles.error,
  success: state.userProfiles.success,
});

const styles = StyleSheet.create({
  container: {
    display: 'flex',
    flexDirection: 'row',
    flex: 1,
    position: 'absolute',
    height: '100%',
  },
  iconView: {
    backgroundColor: 'rgba(255,255,255,.25)',
    width: '25%',
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  inputView: {
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
  },
  myProfileView: {
    backgroundColor: 'black',
    width: '75%',
    height: '100%',
    display: 'flex',
    alignItems: 'center',
    paddingTop: 20,
  },
  saveBtnContainer: {
    position: 'relative',
    width: '90%',
  },
});

export default connect(mapStateToProps, {
  storeProfileData, profileDataPending, getProfileData, profileImageClicked,
})(MyProfile);
