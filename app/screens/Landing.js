import React from 'react';
import { StyleSheet, Text, TextInput, View, Button, TouchableOpacity, Image } from 'react-native';
import firebase from 'react-native-firebase';


class Landing extends React.Component {
  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('./../../assets/logo-yw.png')}
          style={styles.imageContainer} />
        <Text style={styles.headerTxt}>Open mic from anywhere</Text>
        <View style={styles.secondaryTxtContainer}>
          <Text style={styles.secondaryHeader}>Join from home. Promote your music</Text>
        </View>
        <View style={styles.btnContainer}>
          <TouchableOpacity style={styles.lgBtn} onPress={() => this.props.navigation.navigate('Login')}>
            <Text style={styles.lgBtnText}>Login</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.suBtn} onPress={() => this.props.navigation.navigate('SignUp')}>
            <Text style={styles.suBtnText}>Sign Up</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  imageContainer: {
    width: '100%',
    height: 150,
  },
  headerTxt: {
    lineHeight: 50,
    fontSize: 34,
    width: '70%',
    color: '#fff',
    textAlign: 'center',
  },
  secondaryHeader: {
    fontSize: 14,
    marginTop: 30,
    color: '#fff',
    width: '80%',
  },
  btnContainer: {
    display: 'flex',
    width: '100%',
    justifyContent: 'space-around',
    flexDirection: 'row',
    position: 'absolute',
    bottom: 10,
  },
  lgBtn: {
    backgroundColor: 'black',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: '#f1d746',
  },
  lgBtnText: {
    color: 'white',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 60,
    paddingRight: 60,
  },
  suBtn: {
    backgroundColor: '#f1d746',
    borderRadius: 5,
  },
  suBtnText: {
    color: 'black',
    paddingTop: 10,
    paddingBottom: 10,
    paddingLeft: 60,
    paddingRight: 60,
  },
});


export default Landing;
