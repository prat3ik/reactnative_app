import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withNavigation } from 'react-navigation';
import firebase from 'react-native-firebase';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  TouchableOpacity,
  Image,
  Animated,
} from 'react-native';
import MyProfile from './MyProfile';


class UserProfile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showMyProfile: false,
    };
  }
  render() {
    return (
      <View style={styles.container}>
        <Icon
          onPress={() => this.setState({ showMyProfile: true })}
          name="bars"
          style={{
            fontSize: 30,
            color: 'white',
            position: 'absolute',
            right: 15,
            top: 20,
          }} />
        <Icon
          name="arrow-left"
          onPress={() => this.props.navigation.navigate('Home')}
          style={{
            fontSize: 30,
            color: 'white',
            position: 'absolute',
            left: 15,
            top: 20,
          }} />
        <View style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}>
          <Image
            source={require('./../../assets/dude.jpg')}
            style={{
              width: 160, height: 160, borderRadius: 100,
            }} />
          <Text style={{
            color: 'white', fontWeight: 'bold', fontSize: 22, marginTop: 20, marginBottom: 10,
          }}>John Doe
          </Text>
          <Text style={{ color: 'white', marginBottom: 10 }}>Wilmington,NC</Text>
          <Text style={{ color: 'white', fontWeight: 'bold' }}>Logged In 2 Hours ago</Text>
        </View>
        <View style={{
          alignSelf: 'flex-start', marginTop: 40, marginLeft: 40,
        }}>
          <View style={{ display: 'flex', flexDirection: 'row', marginBottom: 10 }}>
            <Icon name="globe" style={{ color: 'white', fontSize: 30, marginRight: 10 }} />
            <Text style={{ color: 'white' }}>example@ex.com</Text>
          </View>
          <View style={{ display: 'flex', flexDirection: 'row' }}>
            <Icon name="twitter" style={{ color: 'white', fontSize: 30, marginRight: 10 }} />
            <Text style={{ color: 'white' }}>@micturnApp</Text>
          </View>
        </View>
        {
          this.state.showMyProfile
            ? <MyProfile hideProfile={() => this.setState({ showMyProfile: false })} />
            : null
        }
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  playBtn: {
    backgroundColor: '#f1d746',
    width: '100%',
    height: 80,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default UserProfile;
