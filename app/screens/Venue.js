import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import PropTypes from 'prop-types';
import { OTSession, OTPublisher, OTSubscriber } from 'opentok-react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import config from '../config/config';

export default class Venue extends Component {
  constructor(props) {
    super(props);
    this.apiKey = config.opentok.API_KEY;
    // TEMP - created manually in Opentok dashboard for testing
    // temp token is good for 30 days.
    this.sessionId = '1_MX40NjEyNzcxMn5-MTUzNDEwMTkyNzk3NX5zekJyZ05SYkV2ckN1bmliZ2hxR1drK3Z-fg';
    this.token = 'T1==cGFydG5lcl9pZD00NjEyNzcxMiZzaWc9ZGEzMGFlMzIyN2JmMjdkYmM0OTA3Zjk3YjY5MzU0ZDlkMWY0MjhjODpzZXNzaW9uX2lkPTFfTVg0ME5qRXlOemN4TW41LU1UVXpOREV3TVRreU56azNOWDV6ZWtKeVowNVNZa1YyY2tOMWJtbGlaMmh4UjFkckszWi1mZyZjcmVhdGVfdGltZT0xNTM0MTAxOTU2Jm5vbmNlPTAuODE0MDg4NjYxNzI3ODIyOSZyb2xlPXB1Ymxpc2hlciZleHBpcmVfdGltZT0xNTM2NjkzOTU1JmluaXRpYWxfbGF5b3V0X2NsYXNzX2xpc3Q9';

    this.sessionEventHandlers = {
      sessionConnected: () => {
        console.log('Session connected');
      },
      sessionDisconnected: () => {
        console.log('Session disconnected');
      },
    };
  }

  render() {
    console.log('I am in the venue screen.');
    return (
      <View style={styles.container}>
        <Icon
          name="arrow-left"
          style={styles.leftArrow}
          onPress={() => this.props.navigation.navigate('Home')} />

        <OTSession
          apiKey={this.apiKey}
          sessionId={this.sessionId}
          token={this.token}
          eventHandlers={this.sessionEventHandlers}>
          <OTPublisher style={styles.publisherVideo} />
          <OTSubscriber style={styles.subscriberVideo} />
        </OTSession>
      </View>
    );
  }
}

Venue.propTypes = {
  navigation: PropTypes.shape({
    actions: PropTypes.object,
    'state.key': PropTypes.string,
    'state.routeName': PropTypes.string,
  }),
  'navigation.navigate': PropTypes.func,
};

Venue.defaultProps = {
  'navigation.navigate': null,
  navigation: {},
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#000',
  },
  leftArrow: {
    position: 'absolute',
    top: 20,
    left: 30,
    fontSize: 40,
    color: 'white',
    zIndex: 1000,
  },
  publisherVideo: {
    flex: 1,
    width: 200,
    height: 200,
  },
  subscriberVideo: {
    width: 100,
    height: 100,
  },
});
