import React, { Component } from 'react';
import { withNavigation } from 'react-navigation';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  FlatList,
  StyleSheet,
  TextInput,
  View,
  TouchableOpacity,
  Image,
  Animated,
  ScrollView,
} from 'react-native';
import MyProfile from './MyProfile';
import NavBar from '../components/NavBar/NavBar';
import Button from '../components/Button/Button';
import Text from '../components/TextComponent/Text';
import UserList from '../components/UserList/UserList';
import VenueList from '../components/VenueList/VenueList';
import { watchVenuesData } from '../actions/venueActions';
import { leaders } from '../actions/leaderBoardActions';
import MyButton from '../components/MyButton';

class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      showMyProfile: false,
    };
  }

  componentWillMount() {
    // TODO: rename to loadLeaders() or similar for clarity
    this.props.leaders();
    this.props.watchVenuesData();
  }

  render() {
    // console.log('Home leaderResults:',this.props.leaderResults);
    return (
      <ScrollView style={{ backgroundColor: 'black' }}>
        <View style={styles.container}>
          <NavBar title="Home" />

          <View>
            <Text styleName="h3">Ready to Play?</Text>
          </View>

          <MyButton
            primary
            text="This is some text" />

          <Button
            playNow
            text="PLAY NOW"
            icon="arrow-right"
            onPress={() => this.props.navigation.navigate('Venue')} />

          <View style={styles.space15}>
            <Text styleName="h3">Active Musicians</Text>
            <UserList users={this.props.leaderResults} />
          </View>

          <VenueList venues={this.props.venues} />

          <Icon
            onPress={() => this.setState({ showMyProfile: true })}
            name="bars"
            style={{
              fontSize: 30,
              color: 'white',
              position: 'absolute',
              right: 15,
              top: 20,
            }} />
          {
            this.state.showMyProfile
              ? <MyProfile hideProfile={() => this.setState({ showMyProfile: false })} />
              : null
          }
        </View>
      </ScrollView>
    );
  }
}

Home.propTypes = {
  leaders: PropTypes.func.isRequired,
  leaderResults: PropTypes.arrayOf(PropTypes.object).isRequired,
  watchVenuesData: PropTypes.func.isRequired,
  venues: PropTypes.arrayOf(PropTypes.object).isRequired,
};

const mapStateToProps = state => ({
  leaderResults: state.leaderBoard.leaderboard,
  venues: state.venues.venues,
});

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  space15: {
    marginTop: 15,
    marginBottom: 15,
  },
});

export default withNavigation(connect(mapStateToProps, { leaders, watchVenuesData })(Home));
