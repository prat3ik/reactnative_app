import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  FlatList,
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  TouchableOpacity,
  Image,
  Animated,
} from 'react-native';
import MyProfile from './MyProfile';
import { watchVenuesData } from '../actions/venueActions';

class ListVenues extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      showMyProfile: false,
      // fadeIn: new Animated.Value(0),
      // fadeOut: new Animated.Value(1),
    };
  }

  componentWillMount() {
    this.props.watchVenuesData();
  }

  hideProfile() {
    this.setState({ showMyProfile: false });
  }

  render() {
    return (
      <View style={styles.container}>
        <View>
          <Text style={{
            color: 'white', fontSize: 30, marginBottom: 30, marginTop: 15,
          }}>Venues
          </Text>
        </View>
        <Icon
          onPress={() => this.setState({ showMyProfile: true })}
          name="bars"
          style={{
            fontSize: 30,
            color: 'white',
            position: 'absolute',
            right: 15,
            top: 20,
          }} />
        <FlatList
          data={Object.values(this.props.venues)}
          keyExtractor={(x, i) => i.toString()}
          renderItem={({ item }) =>
            (
              <View style={styles.cardContainer}>
                <Image
                  source={require('./../../assets/songwriting.jpg')}
                  style={styles.imageContainer} />
                <View style={styles.descriptionText}>
                  <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                    <Icon
                      name="user"
                      style={{
                        fontSize: 40,
                        color: 'black',
                      }} />
                    <Text style={{ fontSize: 22, marginLeft: 10 }}>22</Text>
                  </View>
                  <Text style={{
                    color: 'black', fontSize: 28, fontWeight: 'bold', marginVertical: 10,
                  }}>
                    {item.title.toUpperCase()}
                  </Text>
                  <Text style={{ color: 'black', fontSize: 20, marginVertical: 10 }}>
                    {item.description}
                  </Text>
                </View>
              </View>
            )} />
        <TouchableOpacity style={styles.navBtn} onPress={() => this.props.navigation.navigate('Home')}>
          <Text>Back to Home</Text>
        </TouchableOpacity>
        {
          this.state.showMyProfile
            ? <MyProfile hideProfile={() => this.setState({ showMyProfile: false })} />
            : null
        }
      </View>

    );
  }
}

ListVenues.propTypes = {
  watchVenuesData: PropTypes.func,
  venues: PropTypes.shape({
    description: PropTypes.string, title: PropTypes.string,
  }),
  navigation: PropTypes.shape({
    actions: PropTypes.object,
    'state.key': PropTypes.string,
    'state.routeName': PropTypes.string,
  }),
  'navigation.navigate': PropTypes.func,
};

ListVenues.defaultProps = {
  venues: {},
  navigation: {},
  watchVenuesData: null,
  'navigation.navigate': null,
};

// retrieve state of venues from store
const mapStateToProps = state => ({
  venues: state.venues.venues,
});


const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  cardContainer: {
    width: 375,
    backgroundColor: 'white',
    marginBottom: 50,
    borderRadius: 1,
    borderWidth: 1,
    borderColor: 'gold',
  },
  imageContainer: {
    width: '100%',
    height: 200,
  },
  descriptionText: {
    height: 200,
    padding: 20,
    borderTopWidth: 3,
    borderTopColor: 'gold',
  },
  navBtn: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: 'yellow',
    height: 50,
    width: 200,
  },
});

export default connect(mapStateToProps, { watchVenuesData })(ListVenues);
