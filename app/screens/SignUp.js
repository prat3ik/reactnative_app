import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  TouchableOpacity,
} from 'react-native';
import Input from '../components/InputComponent/Input';
import Spinner from '../components/SpinnerComponent/Spinner';
import { storeProfileData } from '../actions/userProfileActions';

class SignUp extends React.Component {
  state = {
    email: '',
    password: '',
    fullName: '',
    errorMessage: null,
    loading: false,
  }

  handleSignUp = () => {
    const { email, password, fullName } = this.state;
    this.setState({ loading: true });
    firebase
      .auth()
      .createUserAndRetrieveDataWithEmailAndPassword(email, password)
      .then((response) => {
        const profileData = {
          userId: response.user.uid,
          fullName,
          website: '',
          twitter: '',
          location: '',
          lastLogin: response.user.metadata.lastSignInTime,
        };

        this.storeProfileData(profileData);
        this.navigate();
      })
      .catch(error => this.setState({ errorMessage: error.message, loading: false }));
  }

  storeProfileData(profileData) {
    this.props.storeProfileData(profileData);
  }

  navigate() {
    this.props.navigation.navigate('Home');
  }

  showSpinner() {
    if (this.state.loading) {
      return <Spinner />;
    }

    return false;
  }

  render() {
    return (
      <View style={styles.container}>
        <Icon
          name="arrow-left"
          style={styles.leftArrow}
          onPress={() => this.props.navigation.navigate('Landing')} />
        <View style={styles.headerTextContainer}>
          <Text style={styles.headerText}>SIGN UP</Text>
          <Text style={styles.leaderText}>Create an account and start playing</Text>
        </View>
        {
          this.state.errorMessage &&
          <Text style={{
            color: 'red',
          }}>
            {this.state.errorMessage}
          </Text>
        }
        <Input
          placeholder="Full Name"
          value={this.state.fullName}
          onChangeText={fullName => this.setState({ fullName })} />
        <Input
          placeholder="Email"
          onChangeText={email => this.setState({ email })}
          value={this.state.email} />
        <Input
          secureText
          placeholder="Password"
          onChangeText={password => this.setState({ password })}
          value={this.state.password} />
        {this.showSpinner()}
        <Text style={{ color: 'red' }}>{this.state.errorMessage}</Text>
        <TouchableOpacity
          style={styles.createBtn}
          onPress={this.handleSignUp.bind(this)}>
          <Text>CREATE</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

SignUp.propTypes = {
  storeProfileData: PropTypes.func.isRequired,
  navigation: PropTypes.shape({
    actions: PropTypes.object,
    'state.key': PropTypes.string,
    'state.routeName': PropTypes.string,
  }),
  'navigation.navigate': PropTypes.func,
};

SignUp.defaultProps = {
  navigation: {},
  'navigation.navigate': null,
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftArrow: {
    position: 'absolute',
    top: 20,
    left: 30,
    fontSize: 40,
    color: 'white',
  },
  headerTextContainer: {
    position: 'absolute',
    top: 80,
  },
  headerText: {
    fontSize: 24,
    color: 'white',
    textAlign: 'center',
    marginBottom: 15,
  },
  leaderText: {
    color: 'white',
  },
  createBtn: {
    backgroundColor: '#f1d746',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 60,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default connect(null, { storeProfileData })(SignUp);
