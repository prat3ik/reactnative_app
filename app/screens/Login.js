import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import firebase from 'react-native-firebase';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
  StyleSheet,
  Text,
  TextInput,
  View,
  Button,
  TouchableOpacity,
} from 'react-native';
import Input from '../components/InputComponent/Input';
import Spinner from '../components/SpinnerComponent/Spinner';
import { storeProfileData } from '../actions/userProfileActions';

class Login extends React.Component {
  state = {
    email: '',
    password: '',
    errorMessage: null,
    loading: false,
  }

  handleLogin = () => {
    const { email, password } = this.state;
    this.setState({ loading: true });
    firebase
      .auth()
      .signInAndRetrieveDataWithEmailAndPassword(email, password)
      .then((response) => {
        const profileData = {
          userId: response.user.uid,
          lastLogin: response.user.metadata.lastSignInTime,
        };

        this.storeProfileData(profileData);
        this.navigate();
      })
      .catch(error => this.setState({ errorMessage: error.message, loading: false }));
  }

  storeProfileData(profileData) {
    this.props.storeProfileData(profileData);
    return true;
  }

  navigate() {
    this.props.navigation.navigate('Home');
  }

  showSpinner() {
    if (this.state.loading) {
      return <Spinner />;
    }

    return false;
  }

  changeLoginText() {
    if (this.state.loading) {
      return 'SIGNING IN';
    }
    return 'LOGIN';
  }

  render() {
    return (
      <View style={styles.container}>
        <Icon
          name="arrow-left"
          style={styles.leftArrow}
          onPress={() => this.props.navigation.navigate('Landing')} />
        <View style={styles.headerTextContainer}>
          <Text style={styles.headerText}>LOGIN</Text>
          <Text style={styles.leaderText}>Sign in with you MicTurn account</Text>
        </View>
        {
          this.state.errorMessage &&
          <Text style={{
            color: 'red',
          }}>
            {this.state.errorMessage}
          </Text>
        }
        <Input
          onChangeText={email => this.setState({ email })}
          value={this.state.email}
          placeholder="Email" />
        <Input
          secureText
          onChangeText={password => this.setState({ password })}
          value={this.state.password}
          placeholder="Password" />
        {this.showSpinner()}
        <TouchableOpacity
          style={styles.loginBtn}
          onPress={this
            .handleLogin
            .bind(this)}>
          <Text style={this.state.loading ? styles.signingIn : { color: 'black' }}>{this.changeLoginText()}</Text>
        </TouchableOpacity>
        {/* <Button
          title="Don't have an account? Sign Up"
          onPress={() => this.props.navigation.navigate('SignUp')}/> */}
      </View>
    );
  }
}

Login.propTypes = {
  navigation: PropTypes.shape({
    actions: PropTypes.object,
    'state.key': PropTypes.string,
    'state.routeName': PropTypes.string,
  }),
  'navigation.navigate': PropTypes.func,
  storeProfileData: PropTypes.func.isRequired,
};

Login.defaultProps = {
  'navigation.navigate': null,
  navigation: {},
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
  leftArrow: {
    position: 'absolute',
    top: 20,
    left: 30,
    fontSize: 40,
    color: 'white',
  },
  headerTextContainer: {
    position: 'absolute',
    top: 80,
  },
  headerText: {
    fontSize: 24,
    color: 'white',
    textAlign: 'center',
    marginBottom: 15,
  },
  leaderText: {
    color: 'white',
  },
  loginBtn: {
    backgroundColor: '#f1d746',
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: 60,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  signingIn: {
    color: 'gray',
  },
  // forgotPW: {     color: 'white',     textDecorationLine: "underline",
  // textDecorationStyle: "solid",     textDecorationColor: "white",
  // textDecorationWidth: 2 }
});


export default connect(null, { storeProfileData })(Login);
