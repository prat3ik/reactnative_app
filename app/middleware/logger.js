import firebase from 'react-native-firebase';
import { createLogger } from 'redux-logger';
import actionLogLevelMappping from '../config/actionLogLevel';

/**
 * Logger.
 *
 * @param store
 * @param next
 * @param action
 * @returns {function}
 */
const logger = store => next => (action) => {
  const level = getLogLevel(action.type);
  loggerImplementation(level, action);

  return next(action);
};

/**
 * Determine log verbosity for
 * the given action type.
 *
 * @param actionType
 * @returns {string}
 */
function getLogLevel(actionType) {
  let logLevel = 'log';

  for (const level in actionLogLevelMappping) {
    if (Object.values(actionLogLevelMappping[level]).indexOf(actionType) > -1) {
      logLevel = level;
      break;
    }
  }

  return logLevel;
}

/**
 * Implementation specific logging strategies.
 *
 * @param level {string}
 * @param action {object}
 * @returns {bool}
 */
function loggerImplementation(level, action) {
  if (process.env.NODE_ENV === 'development') {
    // got to be a better way to do this, feels hacky
    const logStatement = `console.${level}(${JSON.stringify(action)})`;
    eval(logStatement);
  } else {
    // firebase analytics implemntation
    const log = firebase.analytics();
    log.logEvent(action.type, { level, payload: action.payload });
  }

  return true;
}


export default logger;
