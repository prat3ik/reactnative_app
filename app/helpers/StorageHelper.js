import firebase from 'react-native-firebase';

/**
 * Upload image to firebase storage. 
 *
 * @param image (content of upload)
 * @param imageName
 * @param reference (root folder in storage bucket) 
 * @param mime
 * @returns {string}
 */
 export function uploadImage(image, imageName, reference, mime = 'image/jpg') {
  return new Promise((resolve, reject) => {
    const imageRef = firebase.storage().ref()
                      .child(reference)
                      .child(imageName);
    
    imageRef.put(image, { contentType: mime }).then((response) => {
      resolve(imageRef.getDownloadURL());
    }).catch((error) => {});
  })
}