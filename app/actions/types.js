/**
 * File simply defines constants.
 */


/**
 * Venues Types.
 */

// action for displaying venues and real-time updates
export const FETCH_VENUES = 'FETCH_VENUES';



/**
 * User Profile Types.
 */

// action for successfully saving a user's profile data
export const STORE_PROFILE_DATA_SUCCESS = 'STORE_PROFILE_SUCCESS';
// action for pending profile data
export const PROFILE_DATA_PENDING = 'PROFILE_DATA_PENDING';
// action error for profile data storing  
export const STORE_PROFILE_DATA_ERROR = 'STORE_PROFILE_ERROR';
// action for successfully getting user's profile data
export const GET_PROFILE_DATA_SUCCESS = 'GET_PROFILE_DATA_SUCCESS';



/**
 * Leaderboard Types.
 */

// action for displaying leaders and real-time updates
export const SHOW_LEADERS = 'SHOW_LEADERS';




