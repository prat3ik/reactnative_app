import firebase from 'react-native-firebase';
import { SHOW_LEADERS } from './types';

/**
 * Action (type) and associated data
 * to be sent to the reducer.
 */
const showLeaders = leaderBoard => ({
  type: SHOW_LEADERS,
  payload: leaderBoard,
});

/**
 * Determine leaders.
 */
const leaders = () => function getLeaders(dispatch) {
  // implentaion for getting
  // 'leaders' might change.
  firebase
    .database()
    .ref('user_profiles')
    .orderByChild('lastLogin') // returns in asc order
    .limitToLast(20)
    .on('value', (snapshot) => {
      // ensure array gets cleared everytime
      const leaderBoard = [];

      snapshot.forEach((child) => {
        const userObject = child.val();
        leaderBoard.unshift(userObject); // make desc order
      });

      dispatch(showLeaders(leaderBoard));
    });
};

export { leaders };
