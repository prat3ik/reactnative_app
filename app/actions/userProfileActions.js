import firebase from 'react-native-firebase';
import ImagePicker from 'react-native-image-picker';
import * as ActionTypes from './types';
import { uploadImage } from '../helpers/StorageHelper';

/**
 * Real-time database root reference.
 */
const rootRef = firebase.database().ref('user_profiles');

/**
 * Action called after successfully storing
 * a user's profile data.
 *
 * @param profileData
 */
const storeProfileSuccess = profileData => ({
  type: ActionTypes.STORE_PROFILE_DATA_SUCCESS,
  payload: profileData,
});

/**
 * Action dispatched when an error has
 * occured storing a user's profile data.
 *
 * @param error
 */
const storeProfileDataError = error => ({
  type: ActionTypes.STORE_PROFILE_DATA_ERROR,
  payload: error,
});

/**
 * Action dispatched when a user profile
 * action is in a pending state.
 */
const profileDataPending = () => ({
  type: ActionTypes.PROFILE_DATA_PENDING,
});

/**
 * Action dispatched after successfully getting
 * a user's profile data.
 *
 * @param profileData
 */
const getProfileDataSuccess = profileData => ({
  type: ActionTypes.GET_PROFILE_DATA_SUCCESS,
  payload: profileData,
});

/**
 * Store profile data by updating
 * a users profile data in database,
 * or create a new database entity if
 * user does not exist.
 *
 * @param profileData
 */
const storeProfileData = profileData => function storeData(dispatch) {
  userExist(profileData.userId).then((profile) => {
    if (!profile) {
      rootRef.push(profileData);
    } else {
      /* eslint no-underscore-dangle: ["error", { "allow": ["_childKeys[0]"] }] */
      const profileRef = firebase.database().ref(`user_profiles/${profile._childKeys[0]}`);
      profileRef.update(profileData);
    }

    dispatch(storeProfileSuccess(profileData));
  }).catch((error) => {
    dispatch(storeProfileDataError(error.message));
  });
};

const profileImageClicked = userId => function imageClicked(dispatch) {
  ImagePicker.launchImageLibrary(null, (response) => {
    if (response.error) {
      dispatch(storeProfileDataError(response.error));
    } else if (response.didCancel) {
      // handle cancel?
    } else {
      dispatch(profileDataPending());

      uploadImage(response.uri, userId, 'user_profile_images').then((imageData) => {
        dispatch(storeProfileData({ userId, imageUrl: imageData }));
      }).catch((error) => {});
    }
  });
};

/**
 * Check if user exists and get
 * their associated profile data.
 *
 * @param userId
 */
const getProfileData = userId => function getData(dispatch) {
  dispatch(profileDataPending());

  userExist(userId).then((profile) => {
    /* eslint no-underscore-dangle: ["error", { "allow": ["_value"] }] */
    const profileData = Object.values(profile._value);
    dispatch(getProfileDataSuccess(profileData[0]));
  }).catch((error) => {
    // handle error when we have more specific data restrictions
  });
};

/**
 * Check if 'user_profiles' has a child
 * value of the currently signed in user's
 * uid, for key 'userId'.
 *
 * @param userId
 * @returns {object}/{string}
 */
function userExist(userId) {
  return new Promise((resolve, reject) => {
    rootRef.orderByChild('userId').equalTo(userId).limitToFirst(1).once('value', (user) => {
      resolve(user);
    })
      .catch((error) => {
        reject(error);
      });
  });
}

export { storeProfileData, getProfileData, profileDataPending, profileImageClicked };

