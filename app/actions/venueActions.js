import firebase from 'react-native-firebase';
import { FETCH_VENUES } from './types';

/**
 * Action (type) and associated data
 * to be sent to the reducer.
 */
const fetchVenues = venueData => ({
  type: FETCH_VENUES,
  payload: venueData,
});

const watchVenuesData = () => function watch(dispatch) {
  firebase.database()
    .ref('venues')
    .on('value', (snapshot) => {
      const venueData = snapshot.val();
      dispatch(fetchVenues(venueData));
    }, error => error);
};

export { fetchVenues, watchVenuesData };
