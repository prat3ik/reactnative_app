import React, { Component } from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  View,
  ViewPropTypes
} from 'react-native';
import PropTypes from 'prop-types';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
// import commonStyles from './Styles';
// import stylesWrapper from './StyleWrapper';

const viewPropTypes = ViewPropTypes || View.propTypes;

export default class Button extends Component {
  static propTypes = {
    icon: PropTypes.string,
    profileSave:PropTypes.bool,
    playNow:PropTypes.bool,
    onPress: PropTypes.func,
    buttonStyles:viewPropTypes.style,
    textStyles:viewPropTypes.style,
    iconStyles:viewPropTypes.style
  }

  static defaultProps = {
    icon: null,
    profileSave:false,
    playNow:false,
    onPress:null,
    buttonStyles:null,
    textStyles:null,
    iconStyles:null
  }

  render() {
    const props = this.props;

    let _icon = null;

    const buttonStyles = [styles.default];
    const textStyles = [styles.defaultText];
    const iconStyles = [styles.defaultIcon];

    if (props.profileSave) {
      buttonStyles.push(styles.profileSave);
    } else if (props.playNow) {
      buttonStyles.push(styles.playNow);
      textStyles.push(styles.playNowText);
      iconStyles.push(styles.playNowIcon);
    }

    if(props.buttonStyles){
      buttonStyles.push(props.buttonStyles)
    }

    if(props.textStyles){
      textStyles.push(props.textStyles)
    }

    if(props.iconStyles){
      iconStyles.push(props.iconStyles)
    }
    
    if (props.icon) {
      _icon = (
        <IconFontAwesome
          name={props.icon}
          style={iconStyles} />
      );
    }

    return (
      <TouchableOpacity style={buttonStyles} onPress={props.onPress}>
        <Text style={textStyles}>{props.text}</Text>
        {_icon}
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  default: {
    backgroundColor: '#f1d746',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  defaultText: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  defaultIcon: {
    fontSize: 15,
    padding: 10,
  },
  profileSave: {
    backgroundColor: '#f1d746',
    width: '100%',
    height: 60,
  },
  playNow: {
    width: '80%',
    height: 80,
    flexDirection: 'row',
  },
  playNowText: {
    fontSize: 18,
  },
});
