import React from 'react';
import commonStyles from './Styles';

const translateProps = (props) => {
  const _key = props.name;
  let _buttonStyles = { ...commonStyles.default };
  const _containerStyles = { ...commonStyles.container };

  if (commonStyles[_key]) {
    _buttonStyles = { ..._buttonStyles, ...commonStyles[_key] };
  }

  const translatedProps = {
    ...props,
    buttonStyles: _buttonStyles,
    containerStyles: _containerStyles,
  };

  return translatedProps;
};

export default WrappedComponent => function wrappedRender(args) {
  	return WrappedComponent(translateProps(args));
};
