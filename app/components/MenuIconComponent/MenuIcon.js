import React from 'react';
import { TouchableOpacity, View } from 'react-native';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import stylesWrapper from './StyleWrapper';

const MenuIcon = props => (
  <TouchableOpacity style={props.buttonStyles} onPress={props.onPress}>
    <IconFontAwesome name={props.name} style={{ fontSize: 40 }} />
  </TouchableOpacity>
);

export default stylesWrapper(MenuIcon);
