const styles = {
  default: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    height: 80,
    width: 80,
    borderRadius: 50,
  },
  remove: {
    backgroundColor: 'white',
  },
  user: {
    backgroundColor: '#f1d746',
  },
  gear: {
    backgroundColor: 'gray',
  },
};

export default styles;
