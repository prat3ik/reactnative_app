const styles = {
  containerStyle: {
    backgroundColor: 'white',
    width: '80%',
    marginBottom: 15,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  inputStyles: {
    width: '80%',
    height: 50,
  }
}

export default styles;