import React from 'react';
import { View, Text, TextInput } from 'react-native';
import commonStyles from './Styles';

const Input = (props) => {
  return (
    <View style={commonStyles.containerStyle}>
      <TextInput 
        autoCapitalize={props.capitalize}
        underlineColorAndroid={props.underline ? props.underline : "transparent"}
        secureTextEntry={props.secureText}
        autoCorrect={props.autoCorrect}
        placeholder={props.placeholder}
        value={props.value}
        onChangeText={props.onChangeText}
        style={commonStyles.inputStyles} 
      />
    </View>
  );
}

export default Input;
 