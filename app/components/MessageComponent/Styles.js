const styles = {
  error: {
  	color: 'red'
  },
  success: {
  	color: 'green'
  }
}

export default styles;