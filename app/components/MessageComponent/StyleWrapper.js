import React from 'react';
import commonStyles from './Styles';

const translateProps = (props) => {
  const _key = props.name;
  let _messageStyle = null;

  if (commonStyles[_key]) {
    _messageStyle = { ...commonStyles[_key] };
  }

  const translatedProps = {
    ...props,
    messageStyle: _messageStyle,
  };

  console.log(`prop ${JSON.stringify(translatedProps)}`);
  return translatedProps;
};

export default WrappedComponent => function wrappedRender(args) {
  	return WrappedComponent(translateProps(args));
};
