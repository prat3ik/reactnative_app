import React from 'react';
import { Text, View } from 'react-native';
import stylesWrapper from './StyleWrapper';

const Message = (props) => {
  return (
  	<View>
      <Text style={props.messageStyle}>
          {props.message}  
      </Text>
    </View>
  );
}

export default stylesWrapper(Message);