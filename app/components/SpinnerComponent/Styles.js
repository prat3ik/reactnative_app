const styles = {
	default:{
    justifyContent: 'center',
    alignItems: 'center'
  },
  userProfileImage:{
    borderRadius: 50,
    width: 150,
    height: 150,
    marginVertical: 20
  }
}

export default styles;

