import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import stylesWrapper from './StyleWrapper';

const Spinner = (props) => {
  return(
    <View style={props.spinnerStyles}>
      <ActivityIndicator size={props.spinnerSize}/>
    </View> 
   );
}

export default stylesWrapper(Spinner); 