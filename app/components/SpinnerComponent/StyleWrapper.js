import React from 'react';
import commonStyles from './Styles';

const translateProps = (props) => {
  let _spinnerSize = 'large';
  let _spinnerStyles = {...commonStyles.default};

  if (props.userProfileImage) {
    _spinnerStyles = {..._spinnerStyles, ...commonStyles['userProfileImage']};
  }

  const translatedProps = {
    ...props, 
   	spinnerSize: _spinnerSize,
    spinnerStyles: _spinnerStyles
  }

  return translatedProps;
}

export default (WrappedComponent) => {
  return function wrappedRender(args) {
  	return WrappedComponent(translateProps(args));
  }
}