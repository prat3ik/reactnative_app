import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import Image from '../ImageComponent/Image';

const VenueItem = (props) => {
  const venue = {};

  if (props.venue) {
    _venue = props.venue;
  }

  return (
    <View>
      <View style={styles.cardContainer}>
        <Image
          imageSource={require('./../../../assets/songwriting.jpg')}
          style={styles.imageContainer} />
        <View style={styles.descriptionText}>
          <View style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <IconFontAwesome
              name="user"
              style={{
                fontSize: 40,
                color: 'black',
              }} />
            <Text styleName="venueViewers">22</Text>
          </View>
          <Text styleName="venueTitle">
            {props.venue.title.toUpperCase()}
          </Text>
          <Text styleName="venueDescription">
            {props.venue.description}
          </Text>
        </View>
      </View>
    </View>
  );
};

VenueItem.defaultProps = {
  venue: null,
};

VenueItem.propTypes = {
  venue: PropTypes.object,
};

const styles = {
  default: {
    color: 'white',
    textAlign: 'center',
  },
  cardContainer: {
    width: 375,
    backgroundColor: 'white',
    marginBottom: 50,
    borderRadius: 1,
    borderWidth: 1,
    borderColor: 'gold',
  },
  imageContainer: {
    width: '100%',
    height: 200,
  },
  descriptionText: {
    height: 200,
    padding: 20,
    borderTopWidth: 3,
    borderTopColor: 'gold',
  },
  navBtn: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 10,
    backgroundColor: 'yellow',
    height: 50,
    width: 200,
  },
};

export default VenueItem;
