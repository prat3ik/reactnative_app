import React from 'react';
import {
  Text,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import styled from 'styled-components';

const StyledTouchableOpacity = styled.TouchableOpacity`
  background: ${props => (props.primary ? '#f1d746' : '#ccc')};
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 15;
  padding: 20px;
`;

const MyButton = props => (
  <StyledTouchableOpacity primary={props.primary}>
    <Text>{props.text}</Text>
  </StyledTouchableOpacity>
);

MyButton.defaultProps = {
  text: null,
};

MyButton.propTypes = {
  text: PropTypes.string,
};

export default MyButton;
