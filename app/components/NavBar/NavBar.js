import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import stylesWrapper from './StyleWrapper';
import Text from '../TextComponent/Text';

const NavBar = props => (
  <View style={props.customStyles}>
    <Text styleName="h3">
      {props.title}
    </Text>
  </View>
);

NavBar.defaultProps = {
  title: null,
};

NavBar.propTypes = {
  title: PropTypes.string,
};

export default stylesWrapper(NavBar);
