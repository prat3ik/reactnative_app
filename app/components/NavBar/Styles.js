const styles = {
  default: {
    width: '100%',
    display: 'flex',
    marginBottom: 20,
    paddingTop: 10,
    paddingBottom: 5,
    paddingLeft: 10,
    paddingRight: 10,
    backgroundColor: '#151516',
  },
};


export default styles;
