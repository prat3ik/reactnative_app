import React from 'react';
import commonStyles from './Styles';

const translateProps = (props) => {
  let _customStyles = { ...commonStyles.default };

  /*
    NOTE: if we use this dynamic style lookup method, we should allow it to pass multiple
    styleName(s). Use a map/loop plus spread operator to apply all
  */
  if (commonStyles[props.styleName]) {
    _customStyles = { ..._customStyles, ...commonStyles[props.styleName] };
  }

  const translatedProps = {
    ...props,
    customStyles: _customStyles,
    containerStyle: commonStyles.containerStyle,
  };

  return translatedProps;
};

export default WrappedComponent => function wrappedRender(args) {
  return WrappedComponent(translateProps(args));
};
