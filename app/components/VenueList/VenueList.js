import React from 'react';
import {
  View,
  TouchableOpacity,
  FlatList,
  StyleSheet,
} from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import Text from '../TextComponent/Text';
import VenueItem from '../VenueItem/VenueItem';
import stylesWrapper from './StyleWrapper';

const VenueList = (props) => {
  let _venues = {};

  if (props.venues) {
    _venues = props.venues;
  }

  return (
    <View style={styles.container}>
      <View>
        <Text styleName="h2">Venues</Text>
        <Text styleName="h5">Play live or just hang out and listen</Text>
      </View>
      <FlatList
        data={Object.values(_venues)}
        keyExtractor={(x, i) => i.toString()}
        renderItem={({ item }) => (
          <VenueItem venue={item} />
        )} />
    </View>
  );
};

VenueList.defaultProps = {
  venues: null,
};

VenueList.propTypes = {
  venues: PropTypes.arrayOf(PropTypes.object),
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'black',
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default stylesWrapper(VenueList);
