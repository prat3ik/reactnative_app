import React from 'react';
import commonStyles from './Styles';

const translateProps = (props) => {
  let imageStyles = { ...commonStyles.default };
  let imageSource = require('./../../../assets/nopic.png');

  if (props.source) {
    imageSource = { uri: props.source };
  }

  if (props.mediumCircle) {
    imageStyles = { ...commonStyles.mediumCircle };
  }

  if (props.largeCircle) {
    imageStyles = { ...commonStyles.largeCircle };
  }

  const translatedProps = {
    ...props,
    imageSource,
    imageStyles,
  };

  return translatedProps;
};

export default WrappedComponent => function wrappedRender(args) {
  return WrappedComponent(translateProps(args));
};
