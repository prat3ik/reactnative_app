import React from 'react';
import PropTypes from 'prop-types';
import { Image, TouchableOpacity } from 'react-native';
import stylesWrapper from './StyleWrapper';

const ImageComponent = props => (
  <TouchableOpacity onPress={props.onPress}>
    <Image source={props.imageSource} style={props.imageStyles} />
  </TouchableOpacity>
);

ImageComponent.defaultProps = {
  imageSource: null,
  imageStyles: null,
};

ImageComponent.propTypes = {
  onPress: PropTypes.func.isRequired,
  imageSource: PropTypes.arrayOf(PropTypes.object),
  imageStyles: PropTypes.arrayOf(PropTypes.object),
};

export default stylesWrapper(ImageComponent);
