const styles = {
  default: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    width: 150,
    height: 150,
    marginVertical: 20,
  },
  mediumCircle: {
    borderRadius: 20,
    width: 40,
    height: 40,
  },
  largeCircle: {
    borderRadius: 50,
    width: 100,
    height: 100,
  },
};

export default styles;
