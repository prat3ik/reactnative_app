import React from 'react';
import {
  View,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import stylesWrapper from './StyleWrapper';
import Text from '../TextComponent/Text';
import UserProfileItem from '../UserProfileItem/UserProfileItem';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';

const UserList = (props) => {
  let _users = [];

  if (props.users) {
    _users = props.users;
  }

  return (
    <View style={props.customStyles}>
      <FlatList
        horizontal
        data={Object.values(_users)}
        keyExtractor={(x, i) => i.toString()}
        ListFooterComponent={() => (
          <TouchableOpacity style={props.touchableOpStyle}>
            <Text styleName="more">More</Text>
            <IconFontAwesome name="arrow-right" style={props.moreArrowStyle} />
          </TouchableOpacity>
        )}
        renderItem={({ item }) => (
          <UserProfileItem
            imageSource={item.imageUrl}
            fullName={item.fullName}
            location={item.location} />
        )} />
    </View>
  );
};

export default stylesWrapper(UserList);
