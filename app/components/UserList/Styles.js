const styles = {
  default: {
    display: 'flex',
    flexDirection: 'row',
  },
  touchableOpStyle: {
    display: 'flex',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  moreArrowStyle: {
    fontSize: 20,
    color: 'yellow',
  },
};


export default styles;
