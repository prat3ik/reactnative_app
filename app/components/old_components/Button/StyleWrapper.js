import React from 'react';
import { Animated } from 'react-native';
import commonStyles from './Styles';

const translateProps = (props) => {
  let _buttonStyles = null;
  let _textStyles = null;
  let _iconStyles = null;

  if (props.profileSave) {
    _buttonStyles = { ...commonStyles.profileSave };
  } else if (props.playNow) {
    _buttonStyles = { ...commonStyles.playNow };
    _textStyles = { ...commonStyles.playNowText };
    _iconStyles = { ...commonStyles.playNowIcon };
  }

  const translatedProps = {
    ...props,
    buttonStyles: { ...commonStyles.default, ..._buttonStyles },
    textStyles: { ...commonStyles.defaultText, ..._textStyles },
    iconStyles: { ...commonStyles.defaultIcon, ..._iconStyles },
  };

  return translatedProps;
};

export default WrappedComponent => function wrappedRender(args) {
  	return WrappedComponent(translateProps(args));
};
