import React from 'react';
import {
  Text,
  TouchableOpacity,
} from 'react-native';
import PropTypes from 'prop-types';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import commonStyles from './Styles';
import stylesWrapper from './StyleWrapper';

const Button = (props) => {
  let _icon = null;

  if (props.icon) {
    _icon = (
      <IconFontAwesome
        name={props.icon}
        style={props.iconStyles} />
    );
  }

  return (
    <TouchableOpacity style={props.buttonStyles} onPress={props.onPress}>
      <Text style={props.textStyles}>{props.text}</Text>
      {_icon}
    </TouchableOpacity>
  );
};

Button.defaultProps = {
  icon: null,
};

Button.propTypes = {
  icon: PropTypes.string,
};

export default stylesWrapper(Button);
