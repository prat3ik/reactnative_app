const styles = {
  default: {
    backgroundColor: '#f1d746',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  defaultText: {
    fontSize: 15,
    fontWeight: 'bold',
  },
  defaultIcon: {
    fontSize: 15,
    padding: 10,
  },
  profileSave: {
    backgroundColor: '#f1d746',
    width: '100%',
    height: 60,
  },
  playNow: {
    width: '80%',
    height: 80,
    flexDirection: 'row',
  },
  playNowText: {
    fontSize: 18,
  },
};

export default styles;
