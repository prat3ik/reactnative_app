import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import stylesWrapper from './StyleWrapper';

const TextComponent = props => (
  <View>
    <Text style={props.customStyles}>
      {props.children}
    </Text>
  </View>
);

TextComponent.defaultProps = {
  customStyles: null,
  children: null,
};

TextComponent.propTypes = {
  customStyles: PropTypes.arrayOf(PropTypes.object),
  children: PropTypes.string,
};

export default stylesWrapper(TextComponent);
