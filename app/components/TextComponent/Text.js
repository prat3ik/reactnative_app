import React, { Component } from 'react';
import {
  Text,
  StyleSheet,
  View,
  ViewPropTypes
} from 'react-native';
import PropTypes from 'prop-types';

const viewPropTypes = ViewPropTypes || View.propTypes;

export default class TextComponent extends Component {
  static propTypes = {
    textStyles:viewPropTypes.style,
    customStyles:PropTypes.arrayOf(PropTypes.object),
    children: PropTypes.string,
  }

  static defaultProps = {
    textStyles:null,
    customStyles:null,
    children: null,
  }

  render() {
    const props = this.props;

    const textStyles = [styles.default];
    
    if (styles[props.styleName]) {
    	textStyles.push(styles[props.styleName])
  	}

  	if(props.textStyles){
      textStyles.push(props.textStyles)
    }

    return (
      <View>
	    <Text style={textStyles}>
	      {props.children}
	    </Text>
	  </View>
    );
  }
}

const styles = StyleSheet.create({
  default: {
    color: 'white',
    textAlign: 'center',
    fontSize: 10,
    padding: 3,
  },
  h1: {
    fontSize: 25,
    padding: 20,
    fontWeight: 'bold',
  },
  h2: {
    fontSize: 20,
    padding: 20,
    fontWeight: 'bold',
  },
  h3: {
    fontSize: 15,
    padding: 20,
    fontWeight: 'bold',
  },
  h4: {
    fontSize: 14,
  },
  h5: {
    fontSize: 13,
  },
  h6: {
    fontSize: 12,
  },
  h7: {
    fontSize: 11,
  },
  more: {
    fontSize: 20,
    color: 'yellow',
  },
  mediumBtnText: {
    color: 'black',
    fontSize: 18,
    fontWeight: 'bold',
  },
  venueViewers: {
    color: 'black',
    fontSize: 22,
    paddingLeft: 10,
  },
  venueTitle: {
    color: 'black',
    fontSize: 28,
    fontWeight: 'bold',
    paddingVertical: 10,
  },
  venueDescription: {
    color: 'black',
    fontSize: 20,
    paddingVertical: 10,
  },
});
