import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import Text from '../TextComponent/Text';
import stylesWrapper from './StyleWrapper';
import Image from '../ImageComponent/Image';

const UserProfileItem = props => (
  <View style={props.containerStyle}>
    <Image
      largeCircle
      source={props.imageSource} />
    <View style={props.textContainerStyle}>
      <Text styleName="h6">{props.fullName}</Text>
      <Text styleName="h7">{props.location}</Text>
    </View>
  </View>
);

UserProfileItem.defaultProps = {
  imageSource: null,
  fullName: null,
  location: null,
};

UserProfileItem.propTypes = {
  imageSource: PropTypes.string,
  fullName: PropTypes.string,
  location: PropTypes.string,
};

export default stylesWrapper(UserProfileItem);
