import React from 'react';
import commonStyles from './Styles';

const translateProps = (props) => {
  let _customStyles = { ...commonStyles.default };

  if (commonStyles[props.styleName]) {
    _customStyles = { ..._customStyles, ...commonStyles[props.styleName] };
  }

  const translatedProps = {
    ...props,
    containerStyle: commonStyles.containerStyle,
    textContainerStyle: commonStyles.textContainerStyle,
    customStyles: _customStyles,
  };

  return translatedProps;
};

export default WrappedComponent => function wrappedRender(args) {
  return WrappedComponent(translateProps(args));
};
