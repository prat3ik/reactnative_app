const styles = {
  default: {

  },
  containerStyle: {
    display: 'flex',
    width: 120,
    alignItems: 'center',
  },
  textContainerStyle: {
    marginTop: 10,
  },
};

export default styles;
