import { SwitchNavigator } from 'react-navigation';
import Loading from '../screens/Loading';
import Landing from '../screens/Landing';
import Home from '../screens/Home';
import SignUp from '../screens/SignUp';
import Login from '../screens/Login';
import MyProfile from '../screens/MyProfile';
import ListVenues from '../screens/ListVenues';
import Leaderboard from '../screens/Leaderboard';
import Venue from '../screens/Venue';

const Navigator = SwitchNavigator(
  {
    Loading,
    Landing,
    Home,
    ListVenues,
    SignUp,
    Login,
    MyProfile,
    Venue,
    Leaderboard,
  },
  {
    initialRouteName: 'Loading',
  },
);

export default Navigator;
