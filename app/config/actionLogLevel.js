import * as ActionTypes from '../actions/types';

/**
 * Action and its associated log level.
 * Action must exist in types.js.
 */
const actionLogLevelMappings = {
  info: [
    ActionTypes.SHOW_LEADERS,
    ActionTypes.FETCH_VENUES,
  ],
  warn: [
    // actions that shoud be associated to warnings
  ],
  error: [
    ActionTypes.STORE_PROFILE_DATA_ERROR,
  ],
};

export default actionLogLevelMappings;
