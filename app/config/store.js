import thunk from 'redux-thunk';
import firebase from 'react-native-firebase';
import { createStore, applyMiddleware } from 'redux';
import rootReducer from '../reducers';
import logger from '../middleware/logger';

const initialState = {};
const middleware = [thunk, logger];

// rootReducer being my combined reducers
const store = createStore(rootReducer, initialState, applyMiddleware(...middleware));

export default store;
