/**
 * MicTurn example config
 */
const config = {

  // Opentok
  opentok: {
    API_KEY: '',
    API_SECRET: '',
  },

};

module.exports = config;
