import { SHOW_LEADERS } from '../actions/types';

const initialState = {
  leaderboard: [],
};

export default function (state = initialState, action) {
  switch (action.type) {
  case SHOW_LEADERS:
    return {
      ...state,
      leaderboard: action.payload,
    };
  default:
    return state;
  }
}
