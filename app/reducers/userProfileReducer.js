import * as ActionTypes from '../actions/types';

const initialState = {
  profile: [],
  isLoading:  true,
  error: null,
  success: false
};

export default function (state = initialState, action) {
  switch (action.type) {
    case ActionTypes.STORE_PROFILE_DATA_SUCCESS:
      return {
        ...state,
        profile: action.payload,
        isLoading: false,
        success: true,
        error: null
      };
    case ActionTypes.STORE_PROFILE_DATA_ERROR:
      return {
        ...state,
        isLoading: false,
        error: action.payload,
        success: false
      };
    case ActionTypes.GET_PROFILE_DATA_SUCCESS:
      return {
        ...state,
        profile: action.payload,
        error: null,
        isLoading: false,
        success: false
      };
    case ActionTypes.PROFILE_DATA_PENDING:
      return {
        ...state,
        isLoading: true,
        error: null,
        success: false
      };
    default:
     return state;
  }
}
