import { combineReducers } from 'redux';
import venueReducer from './venueReducer';
import userProfileReducer from './userProfileReducer';
import leaderBoardReducer from './leaderBoardReducer';

/**
 * Combines individual reducers
 * and prepares them to be sent
 * to the store.
 */

export default combineReducers({
  venues: venueReducer,
  userProfiles: userProfileReducer,
  leaderBoard: leaderBoardReducer,
});
