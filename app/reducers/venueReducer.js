import { FETCH_VENUES } from '../actions/types';

const initialState = {
  venues: {},
};

export default function (state = initialState, action) {
  switch (action.type) {
  case FETCH_VENUES:
    return {
      ...state,
      venues: action.payload,
    };
  default:
    return state;
  }
}
